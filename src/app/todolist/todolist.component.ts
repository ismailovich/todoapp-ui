import { Component, OnInit } from '@angular/core';
import { TodoService } from '../todo.service';
import { TodoList } from '../models/todo-list';

@Component({
  selector: 'app-todolist',
  templateUrl: './todolist.component.html',
  styleUrls: ['./todolist.component.css']
})
export class TodoListComponent implements OnInit {
  todoLists: TodoList[] = [];
  selectedTodoList: TodoList = new TodoList();

  constructor() { }

  ngOnInit(): void {
     
  }
}
