import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { TodoList } from './models/todo-list';
import { TodoTask } from './models/todo-task';

@Injectable({
  providedIn: 'root'
})
export class TodoService {
   

  constructor(private http: HttpClient) { }

  getTodoLists(): Observable<TodoList[]> {
    return new Observable<TodoList[]>;
  }

  getTodoList(id: number): Observable<TodoList> {
    return new Observable<TodoList>;
  }

  addTodoList(todoList: TodoList): Observable<TodoList> {
    return new Observable<TodoList>;
  }

  updateTodoList(todoList: TodoList): Observable<any> {
    return new Observable<TodoList>;
  }

  deleteTodoList(id: number): Observable<TodoList> {
    return new Observable<TodoList>;
  }

  getTodoTasks(todoListId: number): Observable<TodoTask[]> {
    return new Observable<TodoTask[]>;
  }

  addTodoTask(todoTask: TodoTask): Observable<TodoTask> {
    return new Observable<TodoTask>;
  }

  updateTodoTask(todoTask: TodoTask): Observable<any> {
    return new Observable<TodoList>;
  }

  deleteTodoTask(id: number): Observable<TodoTask> {
    return new Observable<TodoTask>;
  }
}
