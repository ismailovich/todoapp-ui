export class TodoTask {
  id!: number;
  title!: string;
  description!: string;
  isDone!: boolean;
  todoListId!: number;
}
