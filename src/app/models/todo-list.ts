import { TodoTask } from "./todo-task";

export class TodoList {
  id!: number;
  title!: string;
  description!: string;
  tasks!: TodoTask[];
}
